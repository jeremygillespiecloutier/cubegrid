﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class MobileInput : MonoBehaviour
{

    DebugPanel panel;
    public static String LastSwipeDirection = "";

    // Use this for initialization
    void Start()
    {
        if(GameObject.Find("Canvas")!=null)
            panel = GameObject.Find("Canvas").GetComponent<DebugPanel>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.touchCount == 0)
        {
            rotating = false;
            swipeCancelled = false;
        }
        if(monitorSwipe)
            detectSwipe();
        if(monitorRotation)
            detectRotationZoom();
    }

    Vector2 first;
    public delegate void SwipeDelegate(String swipeDirection);
    SwipeDelegate swipeOccured;
    bool monitorSwipe = false;
    const float MIN_SWIPE_LENGTH = 5;
    bool swipeCancelled = false;

    public void detectSwipe()
    {
        LastSwipeDirection = "";
        if (monitorSwipe && Input.touches.Length == 1)
        {
            String swipeDirection = "";
            Touch touch = Input.GetTouch(0);
            if (touch.phase == TouchPhase.Began)
                first = new Vector2(touch.position.x, touch.position.y);

            if (!swipeCancelled && touch.phase == TouchPhase.Ended)
            {
                Vector2 second = new Vector2(touch.position.x, touch.position.y);
                Vector2 diff = second - first;
                if (diff.magnitude < MIN_SWIPE_LENGTH)
                    swipeDirection = "tap";
                else
                {
                    diff.Normalize();

                    if (diff.y > 0 && diff.x > -0.5f && diff.x < 0.5f)
                        swipeDirection = "up";

                    if (diff.y < 0 && diff.x > -0.5f && diff.x < 0.5f)
                        swipeDirection = "down";

                    if (diff.x < 0 && diff.y > -0.5f && diff.y < 0.5f)
                        swipeDirection = "left";

                    if (diff.x > 0 && diff.y > -0.5f && diff.y < 0.5f)
                        swipeDirection = "right";
                }
                swipeOccured(swipeDirection);
                LastSwipeDirection = swipeDirection;
            }
        }
    }

    public void attachSwipe(SwipeDelegate callback)
    {
        swipeOccured = callback;
        monitorSwipe = true;
    }

    public void detachSwipe()
    {
        monitorSwipe = false;
    }


    bool monitorRotation = false;
    bool rotating = false;
    Vector2 axis1;
    public delegate void RotationDelegate(bool isLeft, float angle);
    RotationDelegate rotationOccured;
    const float MIN_ROTATION_ANGLE = 30;
    Vector3 lastAxis;
    public delegate void ZoomDelegate(float amount);
    ZoomDelegate zoomOccured;
    Boolean monitorZoom = false;

    public void detectRotationZoom()
    {
        if (monitorRotation && Input.touchCount==2)
        {
            Touch touch1 = Input.GetTouch(0);
            Touch touch2 = Input.GetTouch(1);
            if(rotating && (touch1.phase==TouchPhase.Ended || touch2.phase == TouchPhase.Ended))
            {
                Vector2 axis2 = touch2.position - touch1.position;
                float angle1 = Mathf.Atan2(axis1.y, axis1.x);
                float angle2 = Mathf.Atan2(axis2.y, axis2.x);
                float diff = Mathf.Abs(Mathf.Rad2Deg * Mathf.DeltaAngle(angle1, angle2));
                if (diff > 180)
                    diff = 360 - diff;
                if(diff>=MIN_ROTATION_ANGLE)
                    rotationOccured(angle1>angle2, diff);
                if(panel!=null)
                    panel.debug("angle1 " + angle1 + " angle2 " + angle2 + " diff " + diff);
            }
            if(touch1.phase==TouchPhase.Moved || touch2.phase == TouchPhase.Moved)
            {
                Vector2 axis2 = touch2.position - touch1.position;
                float amount = axis2.magnitude - lastAxis.magnitude;
                lastAxis = axis2;
                zoomOccured(amount);
                if(panel!=null)
                    panel.debug("zoom occured: " + amount);
            }
            if (!rotating && (touch1.phase == TouchPhase.Began || touch2.phase == TouchPhase.Began))
            {
                swipeCancelled = true;
                rotating = true;
                axis1 = touch2.position - touch1.position;
                lastAxis = axis1;
            }
        }
    }

    public void attachRotation(RotationDelegate callback)
    {
        monitorRotation = true;
        rotationOccured = callback;
    }

    public void detachRotation()
    {
        monitorRotation = false;
    }

    public void attachZoom(ZoomDelegate callback)
    {
        monitorZoom = true;
        zoomOccured = callback;
    }

    public void detachZoom()
    {
        monitorZoom = false;
    }

}
