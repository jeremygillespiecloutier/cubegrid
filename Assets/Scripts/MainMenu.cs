﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour {

    GameObject player, target;
    Vector3 camDiff = new Vector3(0, 7, 0);
    Level level;
    Pathfinder pathfinder;
    Trajectory trajectory=null;
    public static int CURRENT_LEVEL = -1;

    int count = 0;

	// Use this for initialization
	void Start () {
        player = GameObject.Find("Player");
        level = GameObject.Find("Game").GetComponent<Level>();
        pathfinder = GameObject.Find("Game").GetComponent<Pathfinder>();
        target = GameObject.Find("Target");
        target.GetComponent<Renderer>().enabled = false;
        loadSave();
	}

    void loadSave()
    {
        if (!PlayerPrefs.HasKey("created"))
        {
            PlayerPrefs.SetInt("created", 0);
            PlayerPrefs.SetInt("maxLevel", 0);
            PlayerPrefs.Save();
        }
        int maxLevel = PlayerPrefs.GetInt("maxLevel");
        foreach(GameObject cube in GameObject.FindGameObjectsWithTag("cube"))
        {
            LevelInfo info = cube.GetComponent<LevelInfo>();
            info.blocked = info.levelIndex<0 || info.levelIndex > maxLevel;
            if(info.blocked && info.levelIndex >= 0)
            {
                Color col = info.gameObject.GetComponent<Renderer>().material.color;
                col.a = 0.2f;
                info.gameObject.GetComponent<Renderer>().material.color = col;
            }
            if(!info.blocked && info.levelIndex == CURRENT_LEVEL)
            {
                CURRENT_LEVEL = -1;
                player.transform.position = info.gameObject.transform.position + info.collisionOffset;
            }
        }
    }

    void movePlayer()
    {
        if (trajectory == null && nextCell != null)
        {
            trajectory = pathfinder.findPath(player, nextCell);
            target.GetComponent<Renderer>().enabled = true;
            target.transform.position = nextCell.position;
            nextCell = null;
        }
        if(trajectory==null)
            return;

        Cell next = trajectory.path[0];
        Vector3 nextPos = new Vector3(next.x, next.y, next.z);

        if (!trajectory.nextMoveReady)
        {
            trajectory.nextMoveReady = true;
            trajectory.totalRotation = next.currentLink.rotation;
            trajectory.currentRotation = 0;
        }
        float rotDiff = Time.deltaTime * Game.TRIGGER_ROTATION_SPEED;
        if (trajectory.currentRotation + rotDiff > trajectory.totalRotation)
            rotDiff = trajectory.totalRotation - trajectory.currentRotation;
        trajectory.currentRotation += rotDiff;
        trajectory.target.transform.RotateAround(next.currentLink.pivot, next.currentLink.axis, rotDiff);
        if (Mathf.Abs(trajectory.currentRotation - trajectory.totalRotation) <= Game.EPSILON_ROTATION)
        {
            trajectory.path.RemoveAt(0);
            trajectory.nextMoveReady = false;
            level.adjustPosition(trajectory.target.transform);
            if (trajectory.path.Count == 0)
            {
                trajectory = null;
                target.GetComponent<Renderer>().enabled = false;
                if(nextCell==null)
                    checkCollision();
            }
            if (nextCell != null)
                trajectory = null;
        }
    }

    void checkCollision()
    {
        float difference = (player.transform.position - (levelInfo.transform.position + levelInfo.collisionOffset)).magnitude;
        if (!levelInfo.blocked &&  difference<= Game.EPSILON_TRANSLATION)
        {
            CURRENT_LEVEL = levelInfo.levelIndex;
            UnityEngine.SceneManagement.SceneManager.LoadScene("scene" + levelInfo.levelIndex.ToString());
        }else if (levelInfo.functionnality.Equals("settings"))
        {
            Debug.Log("Settings Loaded");
        }
    }

    LevelInfo levelInfo;
    Cell nextCell;

    // Update is called once per frame
    void Update () {
        Camera.main.transform.position = player.transform.position + camDiff;
        Camera.main.transform.LookAt(player.transform);

        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if(Physics.Raycast(ray, out hit))
            {
                levelInfo = hit.transform.gameObject.GetComponent<LevelInfo>();
                if (levelInfo != null)
                {
                    Vector3 pos = hit.transform.position + hit.normal.normalized;
                    target.transform.position = pos;
                    target.GetComponent<Renderer>().enabled = true;
                    nextCell = level.cellFromCoord(pos);
                }
            }
        }
        movePlayer();
	}
}
