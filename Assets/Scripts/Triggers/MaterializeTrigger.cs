﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaterializeTrigger : BaseTrigger {

    public GameObject target;
    public float materializeTime = 1;

	// Use this for initialization
	void Start () {
        initialize(materialize);
	}

    float progress = 0;
    bool executing = false;

    void materialize()
    {
        progress = 0;
        triggered = true;
        executing = true;
        target.GetComponent<Renderer>().enabled = true;
    }

    public override void prepare()
    {
        Color col = target.GetComponent<Renderer>().material.color;
        Color newCol = new Color(col.r, col.g, col.b, 0);
        target.GetComponent<Renderer>().material.color = newCol;
        target.GetComponent<Renderer>().enabled = false;
    }

    public override void Update()
    {
        base.Update();
        if (executing)
        {
            progress += Time.deltaTime / materializeTime;
            progress = Mathf.Min(progress, 1);
            Color col = target.GetComponent<Renderer>().material.color;
            Color newCol = new Color(col.r, col.g, col.b, progress);
            target.GetComponent<Renderer>().material.color = newCol;
            if (progress >= 1)
            {
                //target.tag = "cube";
                executing = false;
                done();
            }
        }
    }
}
