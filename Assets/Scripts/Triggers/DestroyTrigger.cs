﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyTrigger : BaseTrigger {

    public GameObject target;
    bool removed = false;

    private void Start()
    {
        initialize(removeTarget);
    }

    public override void prepare()
    {

    }

    void removeTarget()
    {
        removed = true;
        GameObject explosion = Instantiate(Resources.Load("Explosion")) as GameObject;
        explosion.transform.position = target.transform.position;
        Cell cell = level.grid[(int)Mathf.Round(target.transform.position.x), (int)Mathf.Round(target.transform.position.y), (int)Mathf.Round(target.transform.position.z)];
        cell.blocked = false;
        Destroy(target);
    }

    float counter = 0;

    public override void Update()
    {
        base.Update();
        if (removed)
        {
            counter += Time.deltaTime;
            if (counter >= 1)
            {
                removed = false;
                counter = 0;
                done();
            }
        }
    }
}
