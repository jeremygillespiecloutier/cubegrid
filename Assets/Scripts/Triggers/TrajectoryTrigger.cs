﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrajectoryTrigger : BaseTrigger {

    public List<Path> paths;
    public int cameraFollowIndex = 0;

    public void Start()
    {
        initialize(createTrajectory);
    }
    public override void prepare()
    {

    }

    void createTrajectory()
    {
        int index = 0;
        foreach (Path p in paths)
        {
            Trajectory traj = new Trajectory(p.gameObject.transform.parent.gameObject, p.rotating);
            if(index==cameraFollowIndex)
                traj.callback = done;
            index++;

            Vector3 pos = traj.target.transform.position;
            Cell start = level.grid[(int)Mathf.Round(pos.x), (int)Mathf.Round(pos.y), (int)Mathf.Round(pos.z)];
            foreach (Vector3 coord in p.coordinates)
            {
                pos += coord;
                Cell other = level.grid[(int)Mathf.Round(pos.x), (int)Mathf.Round(pos.y), (int)Mathf.Round(pos.z)];
                foreach (Link l in start.links)
                {
                    if (l.second == other)
                    {
                        other.currentLink = l;
                        traj.path.Add(other);
                        start = other;
                        break;
                    }
                }
            }
            game.trajectories.Add(traj);
        }
        if (!reusable)
            triggered = true;
    }

}
