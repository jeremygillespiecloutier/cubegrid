﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BaseTrigger : MonoBehaviour {

    public bool triggered = false;
    public bool reusable = false;
    protected Level level;
    protected Game game;
    public bool cameraFollow = false;
    public GameObject cameraTarget;
    public float cameraSpeed = 1f;
    public Vector3 cameraDistance;

    Quaternion startRot, stopRot;
    Vector3 startPos, stopPos;
    float cameraProgress = 0;
    int cameraPhase = 0;

    public delegate void TriggerCallback();
    TriggerCallback callback;

    public void initialize(TriggerCallback callback)
    {
        this.callback = callback;
        game = GameObject.Find("Game").GetComponent<Game>();
        level = GameObject.Find("Game").GetComponent<Level>();
    }

    //Called when level loaded
    public abstract void prepare();

    public virtual void Update()
    {
        if (cameraPhase == 1)
        {
            cameraProgress += Time.deltaTime / cameraSpeed;
            cameraProgress = Mathf.Min(cameraProgress, 1);
            Camera.main.transform.rotation = Quaternion.Lerp(startRot, stopRot, cameraProgress);
            Camera.main.transform.position = Vector3.Lerp(startPos, stopPos, cameraProgress);
            if (cameraProgress >= 1)
            {
                cameraPhase = 2;
                callback();
            }
        }
        else if (cameraPhase == 3)
        {
            cameraProgress += Time.deltaTime / cameraSpeed;
            cameraProgress = Mathf.Min(cameraProgress, 1);
            Camera.main.transform.rotation = Quaternion.Lerp(stopRot, startRot, cameraProgress);
            Camera.main.transform.position = Vector3.Lerp(stopPos, startPos, cameraProgress);
            if (cameraProgress >= 1)
            {
                cameraPhase = 0;
                game.lockPlayer(false, false);
            }
        }
    }

    public void execute()
    {
        if (!triggered || reusable)
        {
            cameraPhase = 0;
            cameraProgress = 0;
            if (cameraFollow)
            {
                game.lockPlayer(true, true);
                cameraPhase = 1;
                startRot = Camera.main.transform.rotation;
                stopRot = Quaternion.LookRotation(-cameraDistance, Camera.main.transform.up);
                startPos = Camera.main.transform.position;
                stopPos = cameraTarget.transform.position + cameraDistance;
            }
            else
                callback();
        }
    }

    public void done()
    {
        if (cameraFollow)
        {
            cameraProgress = 0;
            cameraPhase = 3;
        }
    }

}
