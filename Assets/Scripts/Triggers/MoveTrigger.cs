﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveTrigger : BaseTrigger
{
    public GameObject target;

    private void Start()
    {
        initialize(createMovement);
    }

    public override void prepare()
    {

    }

    void createMovement()
    {
        Cell cell = level.cellFromCoord(target.transform.position);
        Cell up = level.grid[cell.x, cell.y + 1, cell.z];
        Cell left = level.grid[cell.x - 1, cell.y, cell.z];
        Cell right = level.grid[cell.x + 1, cell.y, cell.z];
        Cell down = level.grid[cell.x, cell.y - 1, cell.z];
        Cell upLeft = level.grid[cell.x - 1, cell.y + 1, cell.z];
        Cell upRight = level.grid[cell.x + 1, cell.y + 1, cell.z];
        Cell downLeft = level.grid[cell.x - 1, cell.y - 1, cell.z];
        Cell downRight = level.grid[cell.x + 1, cell.y - 1, cell.z];

        Vector3 newPos = Vector3.zero;
        if (down.occupied && downLeft.occupied && left.reachable)
            newPos = new Vector3(cell.x - 1, cell.y, cell.z);
        else if (down.occupied && downLeft.reachable && !left.reachable)
            newPos = new Vector3(cell.x - 1, cell.y - 1, cell.z);
        else if (up.occupied && upRight.occupied && right.reachable)
            newPos = new Vector3(cell.x + 1, cell.y, cell.z);
        else if (up.occupied && upRight.reachable && !right.reachable)
            newPos = new Vector3(cell.x + 1, cell.y + 1, cell.z);
        else if (right.occupied && downRight.occupied && down.reachable)
            newPos = new Vector3(cell.x, cell.y - 1, cell.z);
        else if (right.occupied && downRight.reachable && !down.reachable)
            newPos = new Vector3(cell.x + 1, cell.y - 1, cell.z);
        else if (left.occupied && upLeft.occupied && up.reachable)
            newPos = new Vector3(cell.x, cell.y + 1, cell.z);
        else if (left.occupied && upLeft.reachable && !up.reachable)
            newPos = new Vector3(cell.x - 1, cell.y + 1, cell.z);

        if (newPos != Vector3.zero)
        {
            Cell targetCell = level.cellFromCoord(newPos);
            foreach (Link link in cell.links)
            {
                if (link.second == targetCell)
                {
                    targetCell.currentLink = link;
                    Trajectory traj = new Trajectory(target, true);
                    traj.callback = done;
                    traj.path.Add(targetCell);
                    game.trajectories.Add(traj);
                    break;
                }
            }
        }
    }

}
