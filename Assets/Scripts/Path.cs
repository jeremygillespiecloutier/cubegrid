﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]

public class Path : MonoBehaviour {

    public Color color=Color.blue;
    public List<Vector3> coordinates=new List<Vector3>();
    public bool rotating = true;
    Material mat;

    // Update is called once per frame
    void Update () {
        gameObject.GetComponent<LineRenderer>().enabled = !Application.isPlaying;
        if (!Application.isPlaying)
        {
            if (mat == null)
            {
                mat = new Material(Shader.Find("Standard"));
            }
            Vector3 start = gameObject.transform.position+Vector3.zero;
            List<Vector3> positions = new List<Vector3>();
            positions.Add(start);
            foreach (Vector3 point in coordinates)
            {
                start += point;
                positions.Add(start + Vector3.zero);
            }
            mat.color = color;
            gameObject.GetComponent<LineRenderer>().transform.localPosition= Vector3.zero;
            gameObject.GetComponent<LineRenderer>().sharedMaterial = mat;
            gameObject.GetComponent<LineRenderer>().SetPositions(positions.ToArray());
            gameObject.GetComponent<LineRenderer>().positionCount = positions.Count;
            gameObject.GetComponent<LineRenderer>().widthMultiplier = .2f;
        }
    }
}
