﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class DebugPanel : MonoBehaviour {

    UnityEngine.UI.Text textUpVector, textDirectionVector, textTrajectories, textDebug;
    [NonSerialized] public Vector3 upVector = Vector3.zero;
    [NonSerialized] public Vector3 directionVector = Vector3.zero;
    [NonSerialized] public int numTrajectories = 0;
    public bool display = true;

    // Use this for initialization
    void Start () {
        textUpVector=GameObject.Find("TextUpVector").GetComponent<Text>();
        textDirectionVector = GameObject.Find("TextDirectionVector").GetComponent<Text>();
        textTrajectories = GameObject.Find("TextTrajectories").GetComponent<Text>();
        textDebug = GameObject.Find("TextDebug").GetComponent<Text>();
    }
	
	// Update is called once per frame
	void Update () {
        textUpVector.text =!display ? "": "Up: (" + (int)Mathf.Round(upVector.x) + ", " + (int)Mathf.Round(upVector.y) + ", " + (int)Mathf.Round(upVector.z) + ")";
        textDirectionVector.text =!display ? "": "Direction: (" + directionVector.x + ", " + directionVector.y + ", " + directionVector.z + ")";
        textTrajectories.text =!display ? "": "Active trajectories: " + numTrajectories;
	}

    String[] lines = new String[15];
    int head = 0;
    int tail = 0;
    int size = 0;
    int counter = 0;

    public void debug(String message)
    {
        if (textDebug != null)
        {
            counter++;
            lines[head] = message + " [" + counter + "]";
            head = (head + 1) % lines.Length;
            if (size < lines.Length)
                size++;
            else
                tail = head;
            String output = "";
            for (int i = 0; i < lines.Length; i++)
            {
                int current = (i + tail) % lines.Length;
                output += lines[current] + "\n";
            }
            textDebug.text = output;
        }
    }
}
