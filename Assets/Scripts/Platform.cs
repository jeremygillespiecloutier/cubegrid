﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Platform only work if same  direction, ex cube-cube-platform-(platform's path)-cube
public class Platform : MonoBehaviour {

    public Vector3 path, engageDiff=new Vector3(0,1,0); // position+engageDifference is where player stands on platform
    Vector3 start, stop;
    public float travelTime = 2f;
    float progress = 0;
    bool waiting = true;
    int direction = 1;
    bool engaging = false, disengaging=false, engaged=false;
    Game game;
    Level level;
    GameObject player;
    Vector3 axis, pivot, effectiveAxis, normalizedPath;


    // Use this for initialization
    void Start () {
        start = transform.position;
        stop = transform.position + path;
        normalizedPath = path.normalized;
        game = GameObject.Find("Game").GetComponent<Game>();
        level = GameObject.Find("Game").GetComponent<Level>();
        player = GameObject.Find("Player");
        axis = Vector3.Cross(engageDiff, normalizedPath);
        effectiveAxis = axis;
        level.cellFromCoord(transform.position).blocked=true;
	}

    float currentRotation = 0;
    bool atStart = false;
	
	// Update is called once per frame
	void Update () {
        if (engaging || disengaging)
        {
            float diff = Time.deltaTime * Game.PLAYER_SPEED;
            diff = Mathf.Min(diff, 90 - currentRotation);
            currentRotation += diff;
            player.transform.RotateAround(pivot, effectiveAxis, diff);
            if (currentRotation >= 90)
            {
                if (engaging)
                {
                    waiting = false;
                    engaged = true;
                    engaging = false;
                }
                else
                {
                    disengaging = false;
                    game.lockPlayer(false, false);
                }
                level.adjustPosition(player.transform);
            }

        }
        if (waiting && engaged)
        {
            if (Input.GetKeyDown(KeyCode.Space) || MobileInput.LastSwipeDirection == "tap")
            {
                atStart = (transform.position - start).magnitude < 0.05f;
                bool goodDirec = (Camera.main.transform.forward.normalized - (atStart ? -normalizedPath : normalizedPath)).magnitude < 0.05f;
                bool goodUp = (Camera.main.transform.up.normalized - engageDiff).magnitude < 0.05f;

                if(goodDirec && goodUp)
                {
                    currentRotation = 0;
                    disengaging = true;
                    engaged = false;
                    pivot = transform.position + 0.5f * engageDiff + (atStart ? -normalizedPath / 2 : normalizedPath / 2);
                    effectiveAxis = atStart ? -axis : axis;
                }
            }
        }
        if(!waiting && !engaging && !disengaging)
        {
            progress += Time.deltaTime / travelTime;
            progress = Mathf.Min(progress, 1);
            if (direction == 1)
                transform.position = Vector3.Lerp(start, stop, progress);
            else if (direction == -1)
                transform.position = Vector3.Lerp(stop, start, progress);

            player.transform.position = transform.position + engageDiff;

            if (progress >= 1)
            {
                level.adjustPosition(player.transform);
                level.adjustPosition(transform);
                progress = 0;
                waiting = true;
                direction *= -1;
                level.cellFromCoord(start).blocked = direction==1;
                level.cellFromCoord(stop).blocked = direction==-1;
            }
        }
	}

    public bool engage(Cell cell)
    {
        if ((cell.position - (transform.position + engageDiff)).magnitude < 0.05f)
        {
            atStart = (transform.position - start).magnitude < 0.05f;
            pivot = transform.position + 0.5f*engageDiff+(atStart?-normalizedPath/2:normalizedPath/2);
            effectiveAxis = atStart ? axis : -axis;
            currentRotation = 0;
            disengaging = engaged = false;
            engaging = true;
            game.lockPlayer(true, false);
        }
        return engaging;
    }
}
