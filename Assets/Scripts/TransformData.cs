﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This class stores information about transforms. It allows to save a provided transform, as well as restore the saved values.
/// It also allows to compare the currently saved transform to another one in order to determine if they are within a range of error (tolerance).
/// </summary>
/// 
public class TransformData{

    public Vector3 position;
    public Quaternion rotation;
    public Vector3 localScale;
    bool stored = false;

    public TransformData()
    {

    }

    public TransformData(GameObject gameObj)
    {
        store(gameObj);
    }

    public TransformData(Transform transform)
    {
        store(transform);
    }

    public TransformData(Vector3 pos, Quaternion rot, Vector3 localScl)
    {
        store(pos, rot, localScl);
    }

    public void store(GameObject gameObj)
    {
        store(gameObj.transform);
    }

    public void store(Transform transform)
    {
        store(transform.position, transform.rotation, transform.localScale);
    }

    public void store(Vector3 pos, Quaternion rot, Vector3 localScl)
    {
        position = pos;
        rotation = rot;
        localScale = localScl;
        stored = true;
    }

    public void restore(GameObject gameObj)
    {
        restore(gameObj.transform);
    }

    public void restore(Transform transform)
    {
        if (stored)
        {
            transform.position = position;
            transform.rotation = rotation;
            transform.localScale = localScale;
        }
    }

}
