﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jump : MonoBehaviour {

    public static float JUMP_TIME = 0.8f;

    [Tooltip("The axis along which the player will rotate when jumping")]
    public Vector3 rotationAxis;
    [Tooltip("The up direction in which the player will jump. Should be non-zero for only one axis, and this value determines height of jump.")]
    public Vector3 up;
    [Tooltip("The number of rotations the cube does while jumping.")]
    public int rotations = 1;
    [Tooltip("The time it takes for the player to complete the jump.")]
    public float jumpTime = Jump.JUMP_TIME;

    [System.NonSerialized]
    public Vector3 start;
    [System.NonSerialized]
    public Vector3 stop;

    Vector2 p1, p2, p3;
    float a, b, c;
    [System.NonSerialized]
    public float distance;
    [System.NonSerialized]
    public Vector3 horiz;
    char axis;

    private void Start()
    {
        start = gameObject.transform.Find("start").transform.position;
        stop = gameObject.transform.Find("stop").transform.position;
        horiz = stop - start;
        distance = horiz.magnitude;
        if (start.x == stop.x)
            axis = 'x';
        else if (start.y == stop.y)
            axis = 'y';
        else if (start.z == stop.z)
            axis = 'z';

        float height = 0;
        if (up.x != 0)
            height = up.x;
        else if (up.y != 0)
            height = up.y;
        else if (up.z != 0)
            height = up.z;


        p1 = new Vector2(0, 0);
        p2 = new Vector2(distance / 2, height);
        p3 = new Vector2(distance, 0);

        // The jump function is ax^2+bx+c.
        a = p1.y / ((p1.x - p2.x) * (p1.x - p3.x)) +
            p2.y / ((p2.x - p1.x) * (p2.x - p3.x)) +
            p3.y / ((p3.x - p1.x) * (p3.x - p2.x));
        b = -p1.y * (p2.x + p3.x) / ((p1.x - p2.x) * (p1.x - p3.x)) -
            p2.y * (p1.x + p3.x) / ((p2.x - p1.x) * (p2.x - p3.x)) -
            p3.y * (p1.x + p2.x) / ((p3.x - p1.x) * (p3.x - p2.x));
        c = p1.y * p2.x * p3.x / ((p1.x - p2.x) * (p1.x - p3.x)) +
            p2.y * p1.x * p3.x / ((p2.x - p1.x) * (p2.x - p3.x)) +
            p3.y * p1.x * p2.x / ((p3.x - p1.x) * (p3.x - p2.x));
    }

    public Vector3 positionForProgress(float progress)
    {
        float percent = progress / 100;
        float currentDistance = distance * percent;
        float height=a * currentDistance * currentDistance + b * currentDistance + c;
        Vector3 ret = Vector3.zero;

        if (up.x != 0)
        {
            if (axis == 'y')
                ret = new Vector3(start.x + height, start.y, start.z + horiz.z * percent);
            else if (axis == 'z')
                ret = new Vector3(start.x + height, start.y + horiz.y * percent, start.z);
        }
        else if (up.y != 0)
        {
            if (axis == 'x')
                ret = new Vector3(start.x, start.y + height, start.z + horiz.z * percent);
            else if (axis == 'z')
                ret = new Vector3(start.x + horiz.x * percent, start.y + height, start.z);
        }
        else if (up.z != 0)
        {
            if (axis == 'x')
                ret = new Vector3(start.x, start.y + horiz.y * percent, start.z + height);
            else if (axis == 'y')
                ret = new Vector3(start.x + horiz.x * percent, start.y, start.z + height);
        }

        return ret;
    }

}
