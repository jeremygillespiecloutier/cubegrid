﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class grid : MonoBehaviour
{
	private Transform trans;
	// Use this for initialization
	void Start ()
	{
		trans = this.GetComponent<Transform>();
	}
	
	// Update is called once per frame
	void Update () {
        if (!Application.isPlaying) {
            Vector3 position = new Vector3(Mathf.Round(this.GetComponent<Transform>().position.x), Mathf.Round(this.GetComponent<Transform>().position.y), Mathf.Round(this.GetComponent<Transform>().position.z));
            this.GetComponent<Transform>().position = position;
	        Vector3 angles = trans.rotation.eulerAngles;
	        trans.rotation = Quaternion.Euler(new Vector3(90*Mathf.Round(angles.x/90),90*Mathf.Round(angles.y/90),90*Mathf.Round(angles.z/90)));
        }
	}
}
