﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pathfinder : MonoBehaviour {

    Level level;

	// Use this for initialization
	void Start () {
        level = GameObject.Find("Game").GetComponent<Level>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public Trajectory findPath(GameObject target, Cell destination)
    {
        foreach (Cell cell in level.grid)
        {
            cell.heuristic = 0;
            cell.currentLink = null;
            cell.previous = null;
        }
        Cell current = level.grid[(int)Mathf.Round(target.transform.position.x),
            (int)Mathf.Round(target.transform.position.y),
            (int)Mathf.Round(target.transform.position.z)];
        List<Cell> reachable = new List<Cell>();
        List<Cell> visited = new List<Cell>();
        reachable.Add(current);
        bool found = false;

        while (reachable.Count != 0)
        {
            float best = float.MaxValue;
            foreach (Cell cell in reachable)
            {
                if (cell.heuristic < best)
                {
                    current = cell;
                    best = cell.heuristic;
                }
            }
            if (current == destination)
            {
                found = true;
                break;
            }
            foreach (Link link in current.links)
            {
                float heuristic = Mathf.Abs(destination.x - link.second.x) + Mathf.Abs(destination.y - link.second.y) +
                                  Mathf.Abs(destination.z - link.second.z);

                if (!visited.Contains(link.second))
                {
                    link.second.currentLink = link;
                    link.second.heuristic = heuristic;
                    link.second.previous = current;
                    reachable.Add(link.second);
                }
            }
            reachable.Remove(current);
            visited.Add(current);
        }
        if (found)
        {
            Trajectory traj = new Trajectory(target, true);
            while (current != null)
            {
                traj.path.Insert(0, current);
                current = current.previous;
            }
            traj.path.RemoveAt(0);
            return traj;
        }
        else
            Debug.Log("No path found");
        return null;
    }
}
