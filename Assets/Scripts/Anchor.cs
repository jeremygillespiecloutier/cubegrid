﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class Anchor : MonoBehaviour {

    public Vector3 offset=Vector3.zero;

	// Use this for initialization
	void Start () {
		
	}

	// Update is called once per frame
	void Update () {
        this.transform.position = transform.parent.position + offset;
	}
}
