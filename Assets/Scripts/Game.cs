﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Trajectory
{
    public List<Cell> path = new List<Cell>();
    public GameObject target;
    public bool rotating;
    public Boolean nextMoveReady = false;
    public Vector3 moveVector;
    public float currentRotation, totalRotation, currentTranslation, totalTranslation;
    public Vector3 initialPosition;
    public TrajectoryCallback callback;

    public Trajectory(GameObject target, bool rotating)
    {
        this.rotating = rotating;
        this.target = target;
        initialPosition = target.transform.position;
    }
}

public delegate void TrajectoryCallback();



public class Game : MonoBehaviour {

    public float CAM_DISTANCE = 5;
    public const float MAX_CAM_DISTANCE = 15, MIN_CAM_DISTANCE = 1, SCROLL_ZOOM_MULTIPLIER = 3, PINCH_ZOOM_MULTIPLIER = -0.005f;
    public const float CAM_ROTATION_TIME = 1f;
    public const float PLAYER_SPEED = 150;
    public const float TRIGGER_ROTATION_SPEED = PLAYER_SPEED;
    public const float TRIGGER_TRANSLATION_SPEED = 2;
    public const float EPSILON_TRANSLATION = 0.01f;
    public const float EPSILON_ROTATION = 0.01f;

    public MobileInput mobileInput;
    String swipeDirection = "";
    float zoomAmount = 0;
    public Vector3 direction = new Vector3(0, 0, 1);
    public Vector3 up = new Vector3(0, 1, 0);
    bool moving = false;
    bool rotating = false;
    private bool jumping = false;
    GameObject player, enemy, placeholder, upSphere, portal;
    DebugPanel debugPanel;
    float progress = 0;
    Vector3 lastDirection, lastUp, newDirection, newUp;
    Quaternion lastRotation, newRotation;
    GameObject lights;
    Level level;
    Pathfinder pathfinder;

    public List<Trajectory> trajectories = new List<Trajectory>();

    void Start() {
        level = gameObject.GetComponent<Level>();
        player = GameObject.Find("Player");
        enemy = GameObject.Find("Enemy");
        portal = GameObject.Find("Portal");
        lights = GameObject.Find("Lights");
        pathfinder = GameObject.Find("Game").GetComponent<Pathfinder>();
        placeholder = GameObject.CreatePrimitive(PrimitiveType.Cube);
        placeholder.GetComponent<Renderer>().enabled = false;
        upSphere = new GameObject();
        debugPanel = GameObject.Find("Canvas").GetComponent<DebugPanel>();
        Camera.main.transform.position = player.transform.position - CAM_DISTANCE * direction;
        Camera.main.transform.LookAt(player.transform.position);
        lights.transform.rotation = Quaternion.LookRotation(newDirection, newUp);

        mobileInput.attachRotation((isLeft, angle) => {
            if (!rotating)
                this.swipeDirection = isLeft ? "twistLeft" : "twistRight";
        });
        mobileInput.attachSwipe((swipeDirection) => {
            if (!rotating && !(swipeDirection=="tap" && inputLocked))
                this.swipeDirection = swipeDirection;
        });
        mobileInput.attachZoom((amount) => {
            this.zoomAmount = amount * PINCH_ZOOM_MULTIPLIER;
        });
    }

    void moveEnemy()
    {
        for (int i = trajectories.Count - 1; i >= 0; i--)
        {
            Trajectory traj = trajectories[i];
            Cell next = traj.path[0];
            Vector3 nextPos = new Vector3(next.x, next.y, next.z);

            if (!traj.nextMoveReady)
            {
                traj.nextMoveReady = true;
                if (traj.rotating)
                {
                    traj.totalRotation = next.currentLink.rotation;
                    traj.currentRotation = 0;
                }
                else
                {
                    traj.moveVector = nextPos - traj.target.transform.position;
                    traj.totalTranslation = traj.moveVector.magnitude;
                    traj.currentTranslation = 0;
                    traj.moveVector.Normalize();
                }
            }
            if (traj.rotating)
            {
                float rotDiff = Time.deltaTime * TRIGGER_ROTATION_SPEED;
                if (traj.currentRotation + rotDiff > traj.totalRotation)
                    rotDiff = traj.totalRotation - traj.currentRotation;
                traj.currentRotation += rotDiff;
                traj.target.transform.RotateAround(next.currentLink.pivot, next.currentLink.axis, rotDiff);
            }
            else
            {
                float transDiff = Time.deltaTime * TRIGGER_TRANSLATION_SPEED;
                if (traj.currentTranslation + transDiff > traj.totalTranslation)
                    transDiff = traj.totalTranslation - traj.currentTranslation;
                traj.currentTranslation += transDiff;
                traj.target.transform.position += traj.moveVector * transDiff;
            }
            if ((traj.rotating && Mathf.Abs(traj.currentRotation - traj.totalRotation) <= Game.EPSILON_ROTATION) || (!traj.rotating && Mathf.Abs(traj.currentTranslation - traj.totalTranslation) <= Game.EPSILON_TRANSLATION))
            {
                traj.nextMoveReady = false;
                level.adjustPosition(traj.target.transform);
                traj.path.RemoveAt(0);
                if (traj.path.Count == 0)
                {
                    trajectories.RemoveAt(i);
                    trajectoryComplete(traj);
                    if (traj.callback != null)
                        traj.callback();
                }
            }
        }
    }

    void trajectoryComplete(Trajectory trajectory)
    {
        Vector3 posStart = trajectory.initialPosition;
        Cell cellStart = level.grid[(int)Mathf.Round(posStart.x), (int)Mathf.Round(posStart.y), (int)Mathf.Round(posStart.z)];
        Vector3 posStop = trajectory.target.transform.position;
        Cell cellStop = level.grid[(int)Mathf.Round(posStop.x), (int)Mathf.Round(posStop.y), (int)Mathf.Round(posStop.z)];

        if (trajectory.target.tag.Equals("wall"))
        {
            cellStart.blocked = false;
            cellStart.reachable = level.cellReachable(cellStart);
            cellStop.blocked = true;
            cellStop.reachable = false;
        } else if (trajectory.target.tag.Equals("cube"))
        {
            cellStart.occupied = false;
            cellStart.reachable = level.cellReachable(cellStart);
            cellStop.occupied = true;
            cellStop.reachable = false;
        } else if (trajectory.target.name.Equals("Player"))
        {
            checkCollision();
            moving = false;
        }
    }

    void rotationInput() {
        newDirection = direction;
        newUp = up;
        newRotation = lights.transform.rotation;
        if (Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.S) || swipeDirection.Equals("up") || swipeDirection.Equals("down"))
        {
            int direc = Input.GetKeyDown(KeyCode.S) ? 1 : -1;
            if (swipeDirection.Equals("up") || swipeDirection.Equals("down"))
                direc = swipeDirection.Equals("down") ? 1 : -1;
            Vector3 cross = Vector3.Cross(newDirection, up);
            upSphere.transform.position = newDirection;
            upSphere.transform.RotateAround(Vector3.zero, cross, direc * 90);
            newDirection = upSphere.transform.position;
            newUp = Vector3.Cross(cross, newDirection);
        }
        else if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.D) || swipeDirection.Equals("left") || swipeDirection.Equals("right"))
        {
            int direc = Input.GetKey(KeyCode.A) ? 1 : -1;
            if (swipeDirection.Equals("left") || swipeDirection.Equals("right"))
                direc = swipeDirection.Equals("left") ? 1 : -1;
            upSphere.transform.position = newDirection;
            upSphere.transform.RotateAround(Vector3.zero, up, direc * 90);
            newDirection = upSphere.transform.position;
        }
        else if (Input.GetKey(KeyCode.Q) || Input.GetKey(KeyCode.E) || swipeDirection.Equals("twistLeft") || swipeDirection.Equals("twistRight"))
        {
            int direc = Input.GetKey(KeyCode.Q) ? 1 : -1;
            if (swipeDirection.Equals("twistLeft") || swipeDirection.Equals("twistRight"))
                direc = swipeDirection.Equals("twistLeft") ? 1 : -1;
            upSphere.transform.position = newUp;
            upSphere.transform.RotateAround(Vector3.zero, direction, direc * 90);
            newUp = upSphere.transform.position;
        }
        else
            return;

        rotating = true;
        swipeDirection = "";
        lastUp = up;
        lastDirection = direction;
        lastRotation = lights.transform.rotation;
        newRotation = Quaternion.LookRotation(newDirection, newUp);
        newUp.Normalize();
        newDirection.Normalize();
        progress = 0;
    }

    void movementInput() {
        int x = (int)Mathf.Round(player.transform.position.x);
        int y = (int)Mathf.Round(player.transform.position.y);
        int z = (int)Mathf.Round(player.transform.position.z);
        if (Input.GetKey(KeyCode.Space) || swipeDirection.Equals("tap"))
        {
            Cell current = level.grid[x, y, z];
            Cell down = level.grid[x - (int)up.x, y - (int)up.y, z - (int)up.z];
            Cell front = level.grid[x + (int)direction.x, y + (int)direction.y, z + (int)direction.z];
            Cell frontDown = level.grid[x + (int)direction.x - (int)up.x, y + (int)direction.y - (int)up.y, z + (int)direction.z - (int)up.z];
            Cell next = frontDown;
            if (frontDown.occupied)
                next = front;

            bool engaging = false;
            foreach (Platform platform in level.platforms)
            {
                engaging = platform.engage(front);
                if (engaging)
                {
                    break;
                }
            }

            if (!engaging && !front.occupied && !front.blocked && down.occupied && !down.blocked && !frontDown.blocked) {

                Trajectory traj = new Trajectory(player, true);

                next.currentLink = level.findLink(current, next);
                traj.path.Add(next);
                moving = true;
                trajectories.Add(traj);
            }
            swipeDirection = "";
        }
    }

    Link linkForMovement(Cell start, Cell stop)
    {
        foreach (Link link in start.links)
            if (link.second == stop)
                return link;
        return null;
    }

    void executeAction() {
        if (rotating)
        {
            progress += Time.deltaTime / CAM_ROTATION_TIME;
            progress = Mathf.Min(1, progress);
            direction = Vector3.Slerp(lastDirection, newDirection, progress);
            up = Vector3.Slerp(lastUp, newUp, progress);
            lights.transform.rotation = Quaternion.Slerp(lastRotation, newRotation, progress);
            if (progress>=1) {
                rotating = false;
                direction = new Vector3((int)Mathf.Round(direction.x), (int)Mathf.Round(direction.y),
                    (int)Mathf.Round(direction.z));
                up = new Vector3((int)Mathf.Round(up.x), (int)Mathf.Round(up.y), (int)Mathf.Round(up.z));
            }
        }
        else if (jumping) {
            float diff = Time.deltaTime * (100f / currentJump.jumpTime);
            if (jumpProgress + diff > 100)
                diff = 100 - jumpProgress;
            jumpProgress += diff;
            player.transform.position = currentJump.positionForProgress(jumpProgress);

            player.transform.RotateAround(player.transform.position, currentJump.rotationAxis, 3.6f * diff * currentJump.rotations);
            if (jumpProgress >= 100) {
                jumping = false;
                level.adjustPosition(player.transform);
            }
        }
    }

    float jumpProgress = 0;
    Jump currentJump;
    TransformData jumpData;

    void checkCollision() {
        foreach (GameObject jump in level.jumps) {
            Vector3 start = jump.transform.Find("start").position;

            if ((player.transform.position - start).sqrMagnitude < 0.1f) {
                currentJump = jump.GetComponent<Jump>();
                jumpProgress = 0;
                jumping = true;
                break;
            }
        }
        Cell current = level.grid[(int)Mathf.Round(player.transform.position.x), (int)Mathf.Round(player.transform.position.y), (int)Mathf.Round(player.transform.position.z)];
        if (current.trigger != null && (current.trigger.reusable || !current.trigger.triggered))
            current.trigger.execute();
        if (current.spiked)
            UnityEngine.SceneManagement.SceneManager.LoadScene(UnityEngine.SceneManagement.SceneManager.GetActiveScene().name);

        if ((player.transform.position - portal.transform.position).magnitude <= EPSILON_TRANSLATION)
        {
            if (MainMenu.CURRENT_LEVEL != -1 && PlayerPrefs.HasKey("created"))
            {
                int maxLevel = PlayerPrefs.GetInt("maxLevel");
                if (MainMenu.CURRENT_LEVEL == maxLevel)
                {
                    PlayerPrefs.SetInt("maxLevel", maxLevel + 1);
                    PlayerPrefs.Save();
                }
            }
            UnityEngine.SceneManagement.SceneManager.LoadScene("mainmenu");
        }
    }

    void checkClick()
    {
        if (1==2 && Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit) && hit.collider.tag.Equals("cube"))
            {
                Vector3 target = hit.transform.position + hit.normal.normalized;
                Trajectory traj=pathfinder.findPath(enemy, level.cellFromCoord(target));
                if (traj != null)
                    trajectories.Add(traj);
            }
        }
    }

    bool inputLocked = false, cameraLocked=false;

    public void lockPlayer(bool inputLocked, bool cameraLocked)
    {
        this.inputLocked = inputLocked;
        this.cameraLocked = cameraLocked;
    }

    void Update() {
        debugPanel.upVector = up;
        debugPanel.directionVector = direction;
        debugPanel.numTrajectories = trajectories.Count;
        foreach (GameObject cube in level.cubes) {
            Color col = cube.GetComponent<Renderer>().material.color;
            cube.GetComponent<Renderer>().material.color = new Color(col.r, col.g, col.b, 1);
        }
        Ray ray = new Ray(Camera.main.transform.position, (player.transform.position - Camera.main.transform.position));
        RaycastHit[] hits = Physics.RaycastAll(ray);
        foreach (RaycastHit hit in hits) {
            if (hit.transform.gameObject.tag.Equals("cube") &&
                hit.distance < (player.transform.position - Camera.main.transform.position).magnitude) {
                Color col = hit.transform.gameObject.GetComponent<Renderer>().material.color;
                hit.transform.gameObject.GetComponent<Renderer>().material.color = new Color(col.r, col.g, col.b, .2f);
            }
        }
        if(Input.GetAxis("Mouse ScrollWheel") != 0)
            zoomAmount= -Input.GetAxis("Mouse ScrollWheel") * SCROLL_ZOOM_MULTIPLIER;
        CAM_DISTANCE += zoomAmount;
        CAM_DISTANCE = Mathf.Max(CAM_DISTANCE, MIN_CAM_DISTANCE);
        CAM_DISTANCE = Mathf.Min(CAM_DISTANCE, MAX_CAM_DISTANCE);
        zoomAmount = 0;
        if (!cameraLocked)
        {
            Camera.main.transform.position = player.transform.position - CAM_DISTANCE * direction;
            Camera.main.transform.LookAt(player.transform, up);
        }
        if (!moving && !rotating && !jumping) {
            if(!inputLocked)
                movementInput();
            if(!cameraLocked)
                rotationInput();
        }
        else {
            executeAction();
        }
        moveEnemy();
        checkClick();
    }

}
