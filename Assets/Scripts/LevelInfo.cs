﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelInfo : MonoBehaviour {
    
    [System.NonSerialized]
    public bool blocked = true;
    public Vector3 collisionOffset = new Vector3(0, 1, 0);//The adjacent cube you need to be on to detect level change
    public int levelIndex=-1;
    public string functionnality = "";

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
