﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cell
{
    public List<Link> links = new List<Link>();
    public int x, y, z;
    public bool occupied = false, blocked = false, spiked = false, reachable=false;
    public Cell previous;
    public float heuristic = 0;
    public Link currentLink;
    public BaseTrigger trigger;
    public Vector3 position{get{return new Vector3(x, y, z);}}
}

public class Link
{
    public Cell first;
    public Cell second;
    public Vector3 pivot;
    public Vector3 axis = Vector3.zero;
    public int rotation;
}

public class Level : MonoBehaviour {

    public List<GameObject> cubes;
    public List<Platform> platforms;
    public Cell[,,] grid;
    public GameObject placeholder;
    public GameObject[] jumps;

    void Awake () {
        placeholder = GameObject.CreatePrimitive(PrimitiveType.Cube);
        placeholder.GetComponent<Renderer>().enabled = false;
        jumps = GameObject.FindGameObjectsWithTag("jump");
        cubes = new List<GameObject>(GameObject.FindGameObjectsWithTag("cube"));
        platforms = new List<Platform>();
        foreach (GameObject o in GameObject.FindGameObjectsWithTag("platform"))
            platforms.Add(o.GetComponent<Platform>());

        foreach (Collider collider in GetComponents<Collider>())
        {
            if (!(collider.gameObject.tag.Equals("cube") || collider.gameObject.tag.Equals("wall") || collider.gameObject.tag.Equals("enemy") || collider.gameObject.name.Equals("Player")))
                collider.enabled = false;
        }

        float minx = float.MaxValue;
        float maxx = float.MinValue;
        float miny = float.MaxValue;
        float maxy = float.MinValue;
        float minz = float.MaxValue;
        float maxz = float.MinValue;

        foreach (GameObject cube in cubes)
        {
            minx = Mathf.Min(minx, cube.transform.position.x);
            maxx = Mathf.Max(maxx, cube.transform.position.x);
            miny = Mathf.Min(miny, cube.transform.position.y);
            maxy = Mathf.Max(maxy, cube.transform.position.y);
            minz = Mathf.Min(minz, cube.transform.position.z);
            maxz = Mathf.Max(maxz, cube.transform.position.z);
        }
        int width = (int)(maxx - minx);
        int height = (int)(maxy - miny);
        int depth = (int)(maxz - minz);
        gameObject.transform.Translate(new Vector3(-minx + 4, -miny + 4, -minz + 4));
        grid = new Cell[width + 8, height + 8, depth + 8];
        for (int i = 0; i < grid.GetLength(0); i++)
        {
            for (int j = 0; j < grid.GetLength(1); j++)
            {
                for (int k = 0; k < grid.GetLength(2); k++)
                {
                    Cell cell = new Cell();
                    cell.x = i;
                    cell.y = j;
                    cell.z = k;
                    grid[i, j, k] = cell;
                }
            }
        }
        foreach (GameObject cube in cubes)
        {
            Cell cell = grid[(int)Mathf.Round(cube.transform.position.x), (int)Mathf.Round(cube.transform.position.y), (int)Mathf.Round(cube.transform.position.z)];
            cell.occupied = true;
        }
        foreach(GameObject wall in GameObject.FindGameObjectsWithTag("wall"))
        {
            Cell cell = grid[(int)Mathf.Round(wall.transform.position.x), (int)Mathf.Round(wall.transform.position.y), (int)Mathf.Round(wall.transform.position.z)];
            cell.blocked = true;
        }
        foreach(GameObject spike in GameObject.FindGameObjectsWithTag("spike"))
        {
            Cell cell = grid[(int)Mathf.Round(spike.transform.position.x), (int)Mathf.Round(spike.transform.position.y), (int)Mathf.Round(spike.transform.position.z)];
            cell.spiked = true;
        }
        foreach(Cell cell in grid)
            cell.reachable=cellReachable(cell);
        foreach (Cell cell in grid)
        {
            if(cell.x>=1 && cell.y>=1 && cell.z>=1 && cell.x<grid.GetLength(0)-1 && cell.y < grid.GetLength(1) - 1 && cell.z < grid.GetLength(2) - 1)
                for (int i=-1;i<=1;i++)
                    for(int j=-1;j<=1;j++)
                        for(int k = -1; k <= 1; k++)
                            if (!(i == 0 && j == 0 && k == 0) && (i == 0 || j == 0 || k == 0))
                            {
                                Vector3 otherPos = new Vector3(cell.x + i, cell.y + j, cell.z + k);
                                Cell other= cellFromCoord(otherPos);
                                if(other.reachable)
                                {
                                    Link link = findLink(cell, other);
                                    if (link != null)
                                        cell.links.Add(link);
                                }
                            }

        }
        foreach (BaseTrigger trigger in GameObject.FindObjectsOfType<BaseTrigger>())
        {
            Cell cell = grid[(int)Mathf.Round(trigger.transform.position.x), (int)Mathf.Round(trigger.transform.position.y), (int)Mathf.Round(trigger.transform.position.z)];
            cell.trigger = trigger;
            trigger.prepare();
        }
    }


    public bool cellReachable(Cell cell)
    {
        int x = cell.x, y = cell.y, z = cell.z;
        bool reachable=cellOccupied(x + 1, y, z);
        reachable|= cellOccupied(x - 1, y, z);
        reachable |= cellOccupied(x, y+1, z);
        reachable |= cellOccupied(x, y-1, z);
        reachable |= cellOccupied(x, y, z+1);
        reachable |= cellOccupied(x, y, z-1);
        return reachable && !cell.occupied && !cell.blocked;
    }

    bool cellOccupied(int x, int y, int z)
    {
        return x >= 0 && y >= 0 && z >= 0 && x < grid.GetLength(0) && y < grid.GetLength(1) && z < grid.GetLength(2) && grid[x, y, z].occupied;
    }

    public struct PivotInfo
    {
        public Vector3 pivot, axis;
        public int rotation;
        public bool found;
    }

    public Link findLink(Cell first, Cell second)
    {
        Vector3 start = new Vector3(first.x, first.y, first.z);
        Vector3 stop = new Vector3(second.x, second.y, second.z);
        PivotInfo info = new PivotInfo();

        int rotation = (stop - start).magnitude > 1.1f ? 180 : 90;
        for (int i = -1; i <= 1; i++)
            for (int j = -1; j <= 1; j++)
                for (int k = -1; k <= 1; k++)
                    if (Mathf.Abs(i) + Mathf.Abs(j) + Mathf.Abs(k) == 2)
                        if ((info = tryPivot(start, stop, new Vector3(start.x + 0.5f * i, start.y + 0.5f * j, start.z + 0.5f * k), rotation)).found)
                            goto loopEnd;
                        loopEnd:
        if (info.found)
        {
            Link link = new Link();
            link.axis = info.axis;
            link.pivot = info.pivot;
            link.rotation = info.rotation;
            link.first = first;
            link.second = second;
            return link;
        }
        return null;
    }

    PivotInfo tryPivot(Vector3 start, Vector3 stop, Vector3 pivot, int rotation)
    {
        PivotInfo info = new PivotInfo();
        info.found = false;

        placeholder.transform.position = start;
        Vector3 axis = new Vector3(1, 0, 0);
        if (start.y == pivot.y)
            axis = new Vector3(0, 1, 0);
        if (start.z == pivot.z)
            axis = new Vector3(0, 0, 1);

        for (int i = 1; i >= -1; i -= 2)
        {
            axis = i * axis;

            placeholder.transform.RotateAround(pivot, axis, -90);
            Cell cell = cellFromCoord(placeholder.transform.position);
            placeholder.transform.RotateAround(pivot, axis, 90);
            if (cell.occupied)
            {
                placeholder.transform.RotateAround(pivot, axis, 90);
                cell = cellFromCoord(placeholder.transform.position);
                if (rotation == 90 && (placeholder.transform.position - stop).magnitude < 0.05f)
                    info.found = true;
                placeholder.transform.RotateAround(pivot, axis, -90);

                placeholder.transform.RotateAround(pivot, axis, 180);
                Cell cell2 = cellFromCoord(placeholder.transform.position);
                if (rotation == 180 && !cell.occupied && (placeholder.transform.position - stop).magnitude < 0.05f)
                    info.found = true;
                placeholder.transform.RotateAround(pivot, axis, -180);

                if (rotation == 90)
                    info.found &= cell2.occupied;
            }
            if (info.found)
            {
                info.pivot = pivot;
                info.axis = axis;
                info.rotation = rotation;
                break;
            }
        }
        return info;
    }

    public Cell cellFromCoord(Vector3 position)
    {
        return grid[(int)Mathf.Round(position.x), (int)Mathf.Round(position.y), (int)Mathf.Round(position.z)];
    }

    public void adjustPosition(Transform transform)
    {
        transform.position = new Vector3(Mathf.Round(transform.position.x), Mathf.Round(transform.position.y), Mathf.Round(transform.position.z));
        float x = Mathf.Round(transform.rotation.eulerAngles.x / 90f) * 90;
        float y = Mathf.Round(transform.rotation.eulerAngles.y / 90f) * 90;
        float z = Mathf.Round(transform.rotation.eulerAngles.z / 90f) * 90;
        transform.rotation = Quaternion.Euler(x, y, z);
    }

}
